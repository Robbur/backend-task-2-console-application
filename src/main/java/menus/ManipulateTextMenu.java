package menus;

import logging.*;

import java.util.*;
import java.io.*;
import java.text.DecimalFormat;

public class ManipulateTextMenu {
	//Variable for the Dracula text file
    final File file = new File("../src/main/resources/files/Dracula.txt");
	
	//Variable for formatting the file size
    final private static DecimalFormat decimalFormat = new DecimalFormat("#.##");
	
	//Variables for function execution time
    long startTime;
    long endTime;
    long duration;

	//Variables for input, user choice and writing to log
    Scanner input = new Scanner(System.in);
    String userChoice;
    FileWriter fr = null;

    public void showManipulateTextMenu(FileWriter writer) {
        System.out.println("----------------------------");
        System.out.println("Options for provided txt file");
        System.out.println("----------------------------");
        fr = writer;

        Scanner input = new Scanner(System.in);
        String userChoice;

        do {
            System.out.println("[1] Show the txt file name");
            System.out.println("[2] Show how big the txt file is");
            System.out.println("[3] Show how many lines the txt file has");
            System.out.println("[4] Check if a specific word exists in the txt file");
            System.out.println("[5] Count how many times a word is found in the txt file");
            System.out.println("[6] Go Back");

            System.out.print("Insert selection: ");

			//Setting userChoice equals to next string given by user
            userChoice = input.next();

			//If the input is not empty and the user has not entered '6'
            if (!userChoice.equals(" ") && !userChoice.equals("6")) {
                switch (userChoice) {
                    case "1":
                        System.out.println("\n||||RESULT||||\n");
                        startTime = System.nanoTime();
						//Running the showFileName function
                        showFileName();
                        endTime = System.nanoTime();
                        duration = (endTime - startTime) / 1000000;
                        System.out.println("\nRuntime: " + duration + "ms");
                        System.out.println("\n||||RESULT||||\n");
						//Logging to log file
                        LoggingService.writeToFile("The user chose to show the txt file name. The function took " + duration + "ms to execute\n", writer);
                        break;
                    case "2":
                        System.out.println("\n||||RESULT||||\n");
                        startTime = System.nanoTime();
						//Running the showFileSize function
                        showFileSize();
                        endTime = System.nanoTime();
                        duration = (endTime - startTime) / 1000000;
                        System.out.println("\nRuntime: " + duration + "ms");
                        System.out.println("\n||||RESULT||||\n");
						//Logging to log file
                        LoggingService.writeToFile("The user chose to show the txt size. The function took " + duration + "ms to execute\n", writer);
                        break;
                    case "3":
                        System.out.println("\n||||RESULT||||\n");
                        startTime = System.nanoTime();
						//Running the showLineCount function
                        showLineCount();
                        endTime = System.nanoTime();
                        duration = (endTime - startTime) / 1000000;
                        System.out.println("\nRuntime: " + duration + "ms");
                        System.out.println("\n||||RESULT||||\n");
						//Logging to log file
                        LoggingService.writeToFile("The user chose to count the lines in the txt file. The function took " + duration + "ms to execute\n", writer);
                        break;
                    case "4":
                        System.out.println("\n");
						//Running the checkWord function
                        checkWord();
                        break;
                    case "5":
                        System.out.println("\n");
						//Running the countWord function
                        countWord();
                        break;
                    default:
                        System.out.println("The selection was invalid!");
                        break;
                }
            }
        } while (!userChoice.equalsIgnoreCase("6"));
		LoggingService.writeToFile("The user went back to the main menu from the manipulate text menu\n", writer);
    }

    public void showFileName() {
        try {
			//Prints out the file variable name
            System.out.println(file.getName());
        } catch (Exception e) {
            System.out.println("Error finding file");
        }
    }

    public void showFileSize() {
        long fileSize = file.length();
        double fileSizeKB = file.length() / 1024.0;

		//Prints out the file variable size in bytes and kilobytes
        try {
            System.out.println("The size of the " + file.getName() + " file is " + fileSize + " bytes, or " + decimalFormat.format(fileSizeKB) + " kilobytes");
        } catch (Exception e) {
            System.out.println("Error finding files");
        }
    }

    public void showLineCount() {
		//Buffered reader reading each line in the text file and counting
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("../src/main/resources/files/Dracula.txt"))) {
            int numberOfLines = 0;
            while ((bufferedReader.readLine()) != null) {
                numberOfLines++;
            }
            System.out.println(file.getName() + " has " + numberOfLines + " lines");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void checkWord() {
        startTime = System.nanoTime();
        boolean foundWord = false;
        Scanner scanner = null;

        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("Error counting word");
        }

        System.out.println("Which word do you want to search for?");
        System.out.print("Insert selection: ");
        userChoice = input.next();

		//If the user has not given a number, the search will start
        if (!userChoice.matches(".*\\d.*")) {
            userChoice = userChoice.toLowerCase().trim();
			
			//Searcing for the given word
            while (scanner != null && scanner.hasNext()) {
                String nextWord = scanner.next().toLowerCase().trim();
                if (nextWord.equals(userChoice)) {
					//If the word is found, set variable foundWord to true, and break out of loop.
                    foundWord = true;
                    break;
                }
            }
			
			//Printing result to console
            System.out.println("\n||||RESULT||||\n");
            if (foundWord) {
                System.out.println("The word '" + userChoice + "' exists in " + file.getName());
            } else {
                System.out.println("The word '" + userChoice + "' does not exists in " + file.getName());
            }
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000;
            System.out.println("\nRuntime: " + duration + "ms");
            System.out.println("\n||||RESULT||||\n");
			//Logging to file
            LoggingService.writeToFile("The user checked for the word '" + userChoice + "'. The function took " + duration + "ms to execute\n", fr);
        } else {
            System.out.println("\n||||RESULT||||\n");
            System.out.println("Please search for a word only! Your search was: " + userChoice);
            System.out.println("\n||||RESULT||||\n");
        }
    }

    public void countWord() {
        startTime = System.nanoTime();
        int count = 0;

        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("Error counting word");
        }

        System.out.println("Which word do you want to count in the file?");
        System.out.print("Insert selection: ");
        userChoice = input.next();

		//If the user has not given a number, the search will start
        if (!userChoice.matches(".*\\d.*")) {
            userChoice = userChoice.toLowerCase().trim();
			
			//Searcing for the given word
            while (scanner != null && scanner.hasNext()) {
                String nextWord = scanner.next().toLowerCase().trim();
                if (nextWord.equals(userChoice)) {
					//Adding 1, each time word is found
                    ++count;
                }
            }
			//Printing result to console
            System.out.println("\n||||RESULT||||\n");
            System.out.println("The word '" + userChoice + "' was found " + count + " times");
            endTime = System.nanoTime();
            duration = (endTime - startTime) / 1000000;
            System.out.println("\nRuntime: " + duration + "ms");
            System.out.println("\n||||RESULT||||\n");
			//Logging to file
            LoggingService.writeToFile("The user searched for the word '" + userChoice + "'. The function took " + duration + "ms to execute\n", fr);

        } else {
            System.out.println("\n||||RESULT||||\n");
            System.out.println("Please search for a word only! Your search was: " + userChoice);
            System.out.println("\n||||RESULT||||\n");
        }
    }
}