package menus;

import logging.*;

import java.util.*;
import java.io.*;

public class MainMenu {
	//Importing the two other menus
    FindFilesMenu findFiles = new FindFilesMenu();
    ManipulateTextMenu manipulateText = new ManipulateTextMenu();
	
	//Variable for the resourcers folder and the logfile
    final File folder = new File("../src/main/resources/files");
    final File logFile = new File("../src/main/log/logFile.txt");
	
	//Variables for counting execution time of functions
    long startTime;
    long endTime;
    long duration;
	
	//User menu input choice variable
    int userChoice = 0;

	//Variables for scanner for input, and filewriter for logging
    Scanner input = new Scanner(System.in);
    FileWriter writer = null;

    public void showMenu() {
		//Creating the filewriter object
        try {
            writer = new FileWriter(logFile);
        } catch (IOException e) {
            System.out.println("Error reading file");
        }
        System.out.println("-----------------");
        System.out.println("File Manipulation");
        System.out.println("-----------------");
        do {
            System.out.println("[1] List all file names");
            System.out.println("[2] Find files by their extension (pdf, txt, png etc..)");
            System.out.println("[3] Manipulate txt file in resources");
            System.out.println("[4] Exit application");

            System.out.print("Insert selection: ");

			//Setting userChoice equals to next integer given by user
            userChoice = input.nextInt();
            switch (userChoice) {
                case 1:
                    System.out.println("\n||||RESULT||||\n");
                    startTime = System.nanoTime();
					//Running the listAllFileNames function, to list all file names in resources folder
                    listAllFileNames();
                    endTime = System.nanoTime();
                    duration = (endTime - startTime) / 1000000;
                    System.out.println("\nRuntime: " + duration + "ms");
                    System.out.println("\n||||RESULT||||\n");
					//Writing to log file
                    LoggingService.writeToFile("The user listed all files. The function took " + duration + "ms to execute\n", writer);
                    break;
                case 2:
                    System.out.println("");
					//Running the FindFilesMenu class and it's function showFindFilesMenu
                    findFiles.showFindFilesMenu(writer);
                    System.out.println("");
                    break;
                case 3:
                    System.out.println("\n");
					//Writing to log file
					LoggingService.writeToFile("The user entered the manipulate text menu\n", writer);
					//Running the ManipulateTextMenu class and it's function showManipulateTextMenu
                    manipulateText.showManipulateTextMenu(writer);
                    System.out.println("\n");
                    break;
                case 4:
                    System.out.println("Shutting down...");
                    System.out.println("");
					//Writing to log file
                    LoggingService.writeToFile("close", writer);
                    break;
                default:
                    System.out.println("The selection was invalid!");
                    break;
            }
        } while (userChoice != 4);
        System.out.println("Application has been shut down");
    }

    public void listAllFileNames() {

        try {
            for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
				//Printing each file name
                System.out.println(fileEntry.getName());
            }
        } catch (Exception e) {
            System.out.println("Could not find any files");
        }
    }
}