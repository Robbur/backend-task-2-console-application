package menus;

import logging.*;

import java.util.*;
import java.io.*;

public class FindFilesMenu {
	//Variable for the resources folder
    final File folder = new File("../src/main/resources/files");
	
	//Variables for function execution time
    long startTime;
    long endTime;
    long duration;

	//Variables for input, user choice and for writing to log
    Scanner input = new Scanner(System.in);
    String userChoice;
    FileWriter writer = null;


    public void showFindFilesMenu(FileWriter writer) {
        System.out.println("----------------------------");
        System.out.println("Find files by extension type");
        System.out.println("----------------------------");
        this.writer = writer;

        do {
            System.out.println("\nType 'exit' to go back!\n");
            System.out.print("Which file extension do you want to find? png, jpeg, txt etc...\n: ");
			
			//Setting userChoice equals to next string given by user
            userChoice = input.next();
			
			//If the input is not empty and the user has not entered 'exit', fun the findFiles function
            if (!userChoice.equals(" ") && !userChoice.equals("exit")) {
                System.out.println("\n||||RESULT||||\n");

                findFiles(userChoice);

                System.out.println("\n||||RESULT||||\n");
            }
        } while (!userChoice.equalsIgnoreCase("exit"));
		//Writing to log file
		LoggingService.writeToFile("The user stopped searching for file extension\n", writer);
    }

    public void findFiles(String fileType) {
        startTime = System.nanoTime();
        boolean foundFile = false;
        try {
            for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
                if (fileEntry.getName().contains("." + fileType.toLowerCase())) {
                    System.out.println(fileEntry.getName());
                    foundFile = true;
                }
            }
        } catch (Exception e) {
            System.out.println("Error finding files with that type");
        }

        if (!foundFile) {
            System.out.println("Found no files with that extension type");
        }
        endTime = System.nanoTime();
        duration = (endTime - startTime) / 1000000;
        System.out.println("\nRuntime: " + duration + "ms");
		//Writing to log file
        LoggingService.writeToFile("The user searched for file extension " + userChoice + ". The function took " + duration + "ms to execute\n", writer);
    }
}