import java.util.*;
import menus.*;

public class ConsoleApplication{
	public static void main(String[] args){
		// Runs the MainMenu.java class, which shows the main menu to the user
		MainMenu mainMenu = new MainMenu();
		mainMenu.showMenu();
	}
}