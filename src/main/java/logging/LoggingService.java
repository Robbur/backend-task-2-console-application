package logging;

import java.io.FileWriter;
import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;

public class LoggingService {
    public static void writeToFile(String data, FileWriter writer) {
		//Variable to format the date and time output
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		//Variable to get current date and time
        LocalDateTime now = LocalDateTime.now();
		
		//If the user has chosen to exit the application
        if (data.equals("close")) {
            try {
				//Write input data to log file
                writer.append(dtf.format(now)).append(": The user closed the application");
            } catch (IOException e) {
                System.out.println("Error writing to file");
            } finally {
                try {
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
		//If the user has NOT chosen to exit the application
        } else {
            try {
				//Write input data to log file
                writer.append(dtf.format(now)).append(": ").append(data);
            } catch (IOException e) {
                System.out.println("Error writing to file");
            }
        }
    }
}