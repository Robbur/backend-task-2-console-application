# Console-application

This is a console application that allows you to search for file extensions/names of files in a folder.
You can also search for words and some other stuff for the txt file in the resources folder.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Prerequisites

What things you need to install the software and how to install them

Install the latest version of Java JDK: https://www.oracle.com/java/technologies/javase-jdk14-downloads.html


### Running the project

Navigate to the project folder and use the following commands to run the project

```bash
javac -d out src/main/java/*.java src/main/java/menus/*.java src/main/java/logging/*.java

cd out

java ConsoleApplication
```
Screenshot: [Compile and run](https://gitlab.com/Robbur/backend-task-2-console-application/-/blob/master/src/main/resources/screenshots/compileAndRun.png)

To create a .jar file and run that, do the following.

```bash
cd out

jar cfe ConsoleApplication.jar ConsoleApplication *.class menus/*.class logging/*.class

java -jar ConsoleApplication.jar

```
Screenshot: [Create Jar File](https://gitlab.com/Robbur/backend-task-2-console-application/-/blob/master/src/main/resources/screenshots/createJarFile.png)
Screenshot: [Run Jar File](https://gitlab.com/Robbur/backend-task-2-console-application/-/blob/master/src/main/resources/screenshots/runJarFile.png)


## Authors

* **Robin Burø** - *Initial work* - [Robbur](https://gitlab.com/Robbur)

